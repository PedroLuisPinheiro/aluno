package br.com.aluno.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MessageException {

    ALUNO_NAO_ENCONTRADO("Aluno não encontrado."),
    NENHUM_ALUNO_REGISTRADO("Nenhum Aluno registrado.");

    private @Getter
    String mensagem;

}
