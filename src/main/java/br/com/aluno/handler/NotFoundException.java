package br.com.aluno.handler;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    String mensagem;

    public NotFoundException(String mensagem) {
        super(mensagem);
    }

}
