package br.com.aluno.service;

import br.com.aluno.model.Aluno;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

public interface AlunoService {

    Aluno findById(Long id) throws ChangeSetPersister.NotFoundException;

    void updateAluno(Long id, Aluno aluno);

    List<Aluno> getAllAlunos();

    Aluno insert(Aluno aluno);

    void delete(Long id);


}
