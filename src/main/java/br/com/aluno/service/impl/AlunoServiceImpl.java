package br.com.aluno.service.impl;

import br.com.aluno.enums.MessageException;
import br.com.aluno.handler.NotFoundException;
import br.com.aluno.model.Aluno;
import br.com.aluno.repository.AlunoRepository;
import br.com.aluno.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlunoServiceImpl implements AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    @Override
    public Aluno findById(Long id) throws NotFoundException {
        Optional<Aluno> aluno = alunoRepository.findById(id);
        return aluno.orElseThrow(() -> new NotFoundException(MessageException.ALUNO_NAO_ENCONTRADO.getMensagem()));
    }

    @Override
    public void updateAluno(Long id, Aluno aluno) {
        Aluno novoAluno = findById(id);

        novoAluno.setIdade(aluno.getIdade());
        novoAluno.setNome(aluno.getNome());
        alunoRepository.save(novoAluno);
    }

    @Override
    public List<Aluno> getAllAlunos() {
        List<Aluno> lstAlunos = alunoRepository.findAll();
        if (lstAlunos.isEmpty() || lstAlunos == null)
            throw new NotFoundException(MessageException.NENHUM_ALUNO_REGISTRADO.getMensagem());
        return lstAlunos;
    }

    @Override
    public Aluno insert(Aluno aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        alunoRepository.deleteById(id);
    }
}
