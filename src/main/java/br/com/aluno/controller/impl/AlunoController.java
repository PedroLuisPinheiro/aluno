package br.com.aluno.controller.impl;

import br.com.aluno.controller.AlunoResource;
import br.com.aluno.model.Aluno;
import br.com.aluno.service.impl.AlunoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/aluno")
public class AlunoController implements AlunoResource {

    @Autowired
    private AlunoServiceImpl alunoServiceImpl;

    @PostMapping(value = "/novo")
    public ResponseEntity<Void> newUser(@Valid @RequestBody Aluno aluno) {
        Aluno obj = alunoServiceImpl.insert(aluno);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(obj.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping(value = "/procurarPorId/{id}")
    public ResponseEntity<Aluno> findById(@PathVariable Long id) throws ChangeSetPersister.NotFoundException {
        Aluno aluno = alunoServiceImpl.findById(id);
        return ResponseEntity.ok().body(aluno);
    }

    @GetMapping(value = "/recuperarTodosAlunos")
    public ResponseEntity<List<Aluno>> getAllAlunos() {
        List<Aluno> alunos = alunoServiceImpl.getAllAlunos();
        return ResponseEntity.ok().body(alunos);
    }

    @DeleteMapping(value = "/deletar/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        alunoServiceImpl.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(value = "/atualizarAluno/{id}")
    public ResponseEntity<Void> updateAluno(@PathVariable Long id, @RequestBody Aluno aluno) {
        alunoServiceImpl.updateAluno(id, aluno);
        return new ResponseEntity(HttpStatus.OK);
    }


}
