package br.com.aluno.controller;

import br.com.aluno.model.Aluno;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AlunoResource {

    ResponseEntity<Void> newUser(Aluno aluno);

    ResponseEntity<Aluno> findById(Long id) throws ChangeSetPersister.NotFoundException;

    ResponseEntity<List<Aluno>> getAllAlunos();

    ResponseEntity deleteById(Long id);

    ResponseEntity<Void> updateAluno(Long id, Aluno aluno);

}
